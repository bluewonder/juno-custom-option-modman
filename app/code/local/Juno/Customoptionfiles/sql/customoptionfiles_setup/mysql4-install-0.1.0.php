<?php
/**
 * install script
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create table 'customoption_product_list'
 */
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('customoptionfiles/product_list')};
CREATE TABLE {$this->getTable('customoptionfiles/product_list')} (
  `id` INT(10) UNSIGNED NOT NULL auto_increment,
  `product_id` INT(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX `IDX_CUSTOM_OPTION_PRODUCT_LIST_PRODUCT_ID` USING BTREE ON {$this->getTable('customoptionfiles/product_list')}( `product_id` )
");

/**
 * create table 'customoption_field'
 */
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('customoptionfiles/field')};
CREATE TABLE {$this->getTable('customoptionfiles/field')} (
  `id` INT(10) UNSIGNED NOT NULL auto_increment,
  `product_id` INT(10) NOT NULL,
  `option_id` INT(10) NOT NULL,
  `field_label` VARCHAR(99) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

/**
 * create table 'customoption_file'
 */
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('customoptionfiles/file')};
CREATE TABLE {$this->getTable('customoptionfiles/file')} (
  `id` INT(10) UNSIGNED NOT NULL auto_increment,
  `field_id` INT(10) NOT NULL,
  `option_value_id` INT(10) NOT NULL,
  `file_name` VARCHAR(99) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `UniqueOptionField` UNIQUE( `field_id`, `option_value_id` )

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX `IDX_CUSTOM_OPTION_FIELD_OPTION_VALUE_ID` USING BTREE ON {$this->getTable('customoptionfiles/file')}( `option_value_id` );
");

$installer->endSetup();