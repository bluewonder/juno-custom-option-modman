<?php

/**
 * @description: Main controller, manage page, editing page
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Adminhtml_customoptionController extends Mage_Adminhtml_Controller_Action
{
    /**
     * init action, highlight menu
     *
     * @return $this
     */

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/customoptionfiles')
            ->_addBreadcrumb(Mage::helper('customoptionfiles')->__('Custom Option Manager'), Mage::helper('customoptionfiles')->__('Custom Option Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initAction();
        $this->renderLayout();
        return $this;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    // edit action
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $productData = Mage::getModel('catalog/product')->load($id);

        if ($productData->getId()) {
            Mage::register('product_data', $productData);

            $this->_initAction();
            $this->_addContent($this->getLayout()->createBlock('customoptionfiles/adminhtml_customoption_edit'))
                ->_addLeft($this->getLayout()->createBlock('customoptionfiles/adminhtml_customoption_edit_tabs'));

            $this->renderLayout();
        } else {
            $this->_getSession()->addError(Mage::helper('customoptionfiles')->__('Product does not exist'));
            $this->_redirect('*/*/');
        }
    }

    // delete action
    public function deleteAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('customoptionfiles/product_list')->getCollection();
        $collection->addFieldToFilter('product_id', array('eq' => $productId));

        foreach ($collection as $item) {
            try {
                $item->delete();
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_getSession()->addSuccess('Delete Successful');
        $this->_redirect('*/*/');
    }

    /**
     * Action of the save button
     */
    public function saveProductAction()
    {
        $params = $this->getRequest()->getParam('products');
        $paramsInArray = Mage::helper('adminhtml/js')->decodeGridSerializedInput($params);

        $resource = Mage::getModel('core/resource');
        /**
         * @var $writeAdapter Varien_Db_Adapter_Pdo_Mysql
         */
        $writeAdapter = $resource->getConnection('core_write');
        if (!empty($paramsInArray)) {
            $products = array_values($paramsInArray);
            $existProduct = Mage::getModel('customoptionfiles/product_list')->getCollection()->getAllProductId();
            $deletedElement = array_diff($existProduct, $products);
            $addedElement = array_diff($products, $existProduct);

            // delete elements
            if (!empty($deletedElement)) {
                $writeAdapter->delete($resource->getTableName('customoptionfiles/product_list'), sprintf('product_id IN (%s)', implode(',', $deletedElement)));
            }
            // add elements
            if (!empty($addedElement)) {
                foreach ($addedElement as $productId) {
                    $model = Mage::getModel('customoptionfiles/product_list')->load(null);
                    $model->setData('product_id', $productId);
                    $model->save();
                }
            }
        } else {
            $writeAdapter->delete($resource->getTableName('customoptionfiles/product_list'));
        }
    }

    public function saveAction()
    {
        try {
            $this->_saveData();
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Error found when saving the custom field'));
            Mage::logException($e);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        }

        $this->_getSession()->addSuccess($this->__('Save successfully'));
        if ($this->getRequest()->getParam('back') == 'edit') {
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        }
        $this->_redirect('*/*/index');
    }

    /**
     * @param $request
     * @return array
     */
    public function _saveData()
    {
        $productId = $this->getRequest()->getParam('id');
        $fieldSaveResult = array();
        $fieldLabel = $this->getRequest()->getParam('fieldlabel');
        if (!empty($fieldLabel)) {
            foreach ($fieldLabel as $optionId => $fields) {
                foreach ($fields as $key => $data) {
                    if (isset($data['label'])) {
                        if (isset($data['is_new']) && $data['is_new'] == 1 && isset($data['delete']) && $data['delete'] == 0) {
                            $model = Mage::getModel('customoptionfiles/field')->load(null);
                            $model->setData('product_id', $productId);
                            $model->setData('option_id', $optionId);
                            $model->setData('field_label', $data['label']);
                            $model->save();
                        } elseif (isset($data['is_new']) && $data['is_new'] == 0) {
                            $model = Mage::getModel('customoptionfiles/field')->load($key);
                            if (!empty($data['delete']) && $data['delete'] == 1) {
                                $model->delete();
                            } else {
                                $model->setData('field_label', $data['label']);
                                $model->save();
                            }
                        }
                        if (!empty($model)) {
                            $fieldSaveResult[$key] = $model;
                        }
                    }
                }
            }
        }

        $fieldvalue = !empty($_FILES['fieldvalue']) ? $_FILES['fieldvalue'] : array();
        $deleteData = $this->getRequest()->getParam('fieldvalue');
        /** @var $helper Juno_Customoptionfiles_Helper_Data */
        $helper = Mage::helper('customoptionfiles');
        if (!empty($fieldvalue)) {
            foreach ($fieldvalue['name'] as $optionId => $values) {
                foreach ($values as $valueId => $value) {
                    foreach ($value as $key => $name) {
                        $model = $fieldSaveResult[$key];
                        if (!empty($model)) {
                            // get file model
                            if ($fileModel = $helper->getFileByFieldAndOptionValue($model->getId(), $valueId)) {
                            } else {
                                $fileModel = Mage::getModel('customoptionfiles/file')->load(null)
                                    ->setData('field_id', $model->getId())
                                    ->setData('option_value_id', $valueId);
                            }

                            if (!empty($deleteData[$optionId][$valueId][$key]['delete']) && $deleteData[$optionId][$valueId][$key]['delete'] == 1) {
                                if ($fileModel->getId())
                                    $fileModel->delete();
                                continue;
                            }
                            if (empty($name['image'])) continue;
                            // Begin create upload object
                            $elementData = array(
                                'name' => $name['image'],
                                'type' => $fieldvalue['type'][$optionId][$valueId][$key]['image'],
                                'tmp_name' => $fieldvalue['tmp_name'][$optionId][$valueId][$key]['image'],
                                'error' => $fieldvalue['error'][$optionId][$valueId][$key]['image'],
                                'size' => $fieldvalue['size'][$optionId][$valueId][$key]['image'],
                            );
                            $result = $helper->validate($elementData);
                            if (!empty($result['error']) && !empty($result['message'])) {
                                $this->_getSession()->addError(
                                    $result['message']
                                );
                                continue;
                            }
                            try {
                                // Upload file
                                $uploader = new Varien_File_Uploader($elementData);
                                $uploader->setAllowedExtensions($helper->getAllowExtension());
                                $path = Mage::getBaseDir('media') . DS . Juno_Customoptionfiles_Model_File::FOLDER_NAME . DS . $model->getId() . DS . $valueId;
                                $fileName = $elementData['name'];
                                $uploadResult = $uploader->save($path, $fileName);
                            } catch (Exception $e) {
                                $this->_getSession()->addError(
                                    $e->getMessage()
                                );
                                continue;
                            }
                            $fileModel->setData('file_name', $uploadResult['file']);
                            $fileModel->save();
                        }
                    }
                }
            }
        }
        return $this;
    }
}