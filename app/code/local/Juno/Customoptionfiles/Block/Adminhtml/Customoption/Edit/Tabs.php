<?php

/**
 * custom option edit page's tabs
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Block_Adminhtml_Customoption_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customoption_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('customoptionfiles')->__('Custom Option'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('customoptionfiles/adminhtml_customoption_edit_tabs_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}