<?php

/**
 * Custom option grid
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Block_Adminhtml_Customoption_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customoptionGrid');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setDefaultDir('ASC');
        $this->setDefaultFilter(array('in_products' => 1));
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $productCollection = $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id');

        $this->setCollection($productCollection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_products',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('catalog')->__('ID'),
                'width' => '50px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('name',
            array(
                'header' => Mage::helper('catalog')->__('Name'),
                'index' => 'name',
            ));

        $this->addColumn('type',
            array(
                'header' => Mage::helper('catalog')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type' => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
            ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name',
            array(
                'header' => Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type' => 'options',
                'options' => $sets,
            ));

        $this->addColumn('sku',
            array(
                'header' => Mage::helper('catalog')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            ));

    }

    /**
     * Retrieve selected products
     *
     * @return array
     */
    public function _getSelectedProducts()
    {
        $customOptionProductListCollection = Mage::getModel('customoptionfiles/product_list')->getCollection();
        return $customOptionProductListCollection->getAllProductId();
    }

    /**
     * Add filter
     *
     * @param object $column
     * @return Juno_Customoptionfiles_Block_Adminhtml_Customoption_Grid
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Retrieve custom option products
     *
     * @return array
     */
    public function getSelectedProducts()
    {
        $products = array();
        $customOptionProductListCollection = Mage::getModel('customoptionfiles/product_list')->getCollection();
        foreach ($customOptionProductListCollection as $product) {
            $products[] = $product->getProductId();
        }
        return $products;
    }

    /**
     * Retrieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Return url for row
     *
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);
        Mage::log($filter);
        if ($filter === "") {
            return '';
        }
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}