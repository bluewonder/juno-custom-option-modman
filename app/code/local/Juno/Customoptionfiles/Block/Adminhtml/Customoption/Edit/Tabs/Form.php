<?php

/**
 * Custom option listing
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Block_Adminhtml_Customoption_Edit_Tabs_Form extends Mage_Core_Block_Template
{
    /**
     * @return Mage_Core_Block_Abstract
     */
    public function _prepareLayout()
    {
        $this->setTemplate('juno/customoption/edit/form.phtml');
    }

    /**
     * get current product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('product_data');
    }

    /**
     * retrieve all options of current product
     *
     * @return array
     */
    public function getAllOptions()
    {
        $product = $this->getProduct();
        $result = array();
        if (isset($product) && $product->getId()) {
            $result = $product->getOptions();
            if (!empty($result)) return $result;
        }
        return $result;
    }

    /**
     * @param $option Mage_Catalog_Model_Product_Option
     * @return string
     */
    public function getOptionHtml($option)
    {
        $block = $this->getLayout()->createBlock('customoptionfiles/adminhtml_customoption_edit_tabs_form_option');
        $block->setOption($option);
        return $block->toHtml();
    }
}