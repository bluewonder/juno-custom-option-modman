<?php

/**
 * container for custom option editing
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Block_Adminhtml_Customoption_Edit_Tabs_Form_Option extends Mage_Adminhtml_Block_Template
{
    /**
     * @return Mage_Core_Block_Abstract|void
     */
    public function _prepareLayout()
    {
        $this->setTemplate('juno/customoption/edit/form/option.phtml');
    }

    /**
     * @return array
     */
    public function getValues()
    {
        $option = $this->getOption();
        $result = array();
        if (!empty($option)) {
            if ($option->getValues()) {
                foreach ($option->getValues() as $item) {
                    $result[] = array(
                        'id' => $item->getId(),
                        'title' => $item->getTitle()
                    );
                }
            } else {
                $result[] = array(
                    'title' => $option->getTitle()
                );
            }
        }
        return $result;
    }

    /**
     * @return Juno_Customoptionfiles_Model_Resource_Field_Collection
     */
    public function getAllField()
    {
        $collection = Mage::getModel('customoptionfiles/field')->getCollection();
        $collection->addFieldToFilter('product_id', array('eq' => $this->getProduct()->getId()));
        $collection->addFieldToFilter('option_id', array('eq' => $this->getOption()->getId()));
        return $collection;
    }

    public function getAllFileByField($fieldId)
    {
        $collection = Mage::getModel('customoptionfiles/file')->getCollection();
        $collection->addFieldToFilter('field_id', array('eq' => $fieldId));
        return $collection;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('product_data');
    }
}