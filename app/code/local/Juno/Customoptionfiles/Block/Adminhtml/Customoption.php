<?php

/**
 * Container for custom option grid
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Block_Adminhtml_Customoption extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_customoption';
        $this->_blockGroup = 'customoptionfiles';
        $this->_headerText = Mage::helper('customoptionfiles')->__('Custom Option Manager');

        $this->_addButton('add', array(
            'label' => $this->__('Save'),
            'onclick' => 'submit()',
            'class' => 'add',
        ));
    }
}