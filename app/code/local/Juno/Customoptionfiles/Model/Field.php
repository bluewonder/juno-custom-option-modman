<?php

/**
 * Field Model
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_Field extends Mage_Core_Model_Abstract
{

    protected $_eventPrefix = 'juno_customoptionfiles_field';

    protected $_eventObject = 'field';

    public function _construct()
    {
        parent::_construct();
        $this->_init('customoptionfiles/field');
    }
}