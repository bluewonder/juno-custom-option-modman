<?php

/**
 * Collection of product list
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_Resource_Product_List_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('customoptionfiles/product_list');
    }

    /**
     * Retrieve all product ids for collection
     *
     * @return array
     */
    public function getAllProductId()
    {
        /**
         * @var $productidsSelect Varien_Db_Select
         */
        $productidsSelect = clone $this->getSelect();
        $productidsSelect->reset(Zend_Db_Select::ORDER);
        $productidsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $productidsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $productidsSelect->reset(Zend_Db_Select::COLUMNS);

        $productidsSelect->columns('product_id', 'main_table');
        return $this->getConnection()->fetchCol($productidsSelect);
    }
}