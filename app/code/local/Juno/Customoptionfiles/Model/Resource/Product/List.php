<?php

/**
 * @description:
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_Resource_Product_List extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('customoptionfiles/product_list', 'id');
    }
}