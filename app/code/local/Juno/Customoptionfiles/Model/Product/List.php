<?php

/**
 * Model product list
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_Product_List extends Mage_Core_Model_Abstract
{

    protected $_eventPrefix = 'juno_customoptionfiles_product_list';

    protected $_eventObject = 'product_list';

    public function _construct()
    {
        parent::_construct();
        $this->_init('customoptionfiles/product_list');
    }
}