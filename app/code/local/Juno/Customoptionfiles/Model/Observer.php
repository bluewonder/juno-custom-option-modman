<?php

/**
 * module observer
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_Observer
{
    /**
     * Delete field then delete all item inside
     *
     * @param $observer
     */
    public function junoCustomoptionfilesFieldDeleteAfter($observer)
    {
        $field = $observer->getField();
        $collection = Mage::getModel('customoptionfiles/file')->getCollection()
            ->addFieldToFilter('field_id', array('eq' => $field->getId()));
        foreach ($collection as $item) {
            $item->delete();
        }
    }

    /**
     * Remove file physically
     *
     * @param $observer
     */
    public function junoCustomoptionfilesFileDeleteAfter($observer)
    {
        $fieldData = $observer->getFile();
        $filePath = Mage::helper('customoptionfiles')->getFilePath($fieldData);
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }

    /**
     * Delete product in product custom option list will delete all field relate to that product' option
     *
     * @param $observer
     */
    public function junoCustomoptionfilesProductListDeleteAfter($observer)
    {
        $item = $observer->getProductList();
        $collection = Mage::getModel('customoptionfiles/field')->getCollection()
            ->addFieldToFilter('product_id', array('eq' => $item->getProductId()));
        foreach ($collection as $item) {
            $item->delete();
        }
    }

    /**
     * Delete product will delete itself from custom option list
     *
     * @param $observer
     */
    public function catalogProductDeleteAfter($observer)
    {
        $product = $observer->getProduct();
        $collection = Mage::getModel('customoptionfiles/product_list')->getCollection()
            ->addFieldToFilter('product_id', array('eq' => $product->getId()));
        foreach ($collection as $item) {
            $item->delete();
        }
    }
}