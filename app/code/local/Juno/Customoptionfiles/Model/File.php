<?php

/**
 * File model
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Model_File extends Mage_Core_Model_Abstract
{
    const FOLDER_NAME = 'files';

    protected $_eventPrefix = 'juno_customoptionfiles_file';

    protected $_eventObject = 'file';

    public function _construct()
    {
        parent::_construct();
        $this->_init('customoptionfiles/file');
    }
}