<?php

/**
 * Module helpers
 *
 * @category   Juno
 * @package    Customoptionfiles
 * @copyright  Copyright (c) 2014 JunoMedia Inc (http://www.junowebdesign.com)
 */
class Juno_Customoptionfiles_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CONFIG_PATH_MAXIMUM_SIZE = 'customoptionfiles/general/maximum_size';
    const CONFIG_PATH_ALLOW_EXTENSION = 'customoptionfiles/general/allowed_extension';

    /**
     * Validate uploaded file data
     *
     * @param $fileData array
     * @return bool
     */
    public function validate($fileData)
    {
        $result = array(
            'error' => false
        );

        if (!empty($fileData['size']) && $fileData['size'] > $this->getMaximumSize()) {
            $result['error'] = true;
            $result['message'] = $this->__(sprintf('Your file "%s" exceed maximum allowed size, please try another one', $fileData['name']));
        }

        return $result;
    }

    /**
     * Get maximum file size for uploading (in byte)
     *
     * @return int
     */
    public function getMaximumSize()
    {
        return intval(Mage::getStoreConfig(self::CONFIG_PATH_MAXIMUM_SIZE));
    }

    /**
     * get allow extension from configuration
     *
     * @return array
     */
    public function getAllowExtension()
    {
        $allowedExtension = Mage::getStoreConfig(self::CONFIG_PATH_ALLOW_EXTENSION);
        if (strpos($allowedExtension, ',') === false) {
            return array($allowedExtension);
        } else {
            return explode(',', $allowedExtension);
        }
    }

    /**
     * get file url
     *
     * @param $fieldData Juno_Customoptionfiles_Model_File
     * @return string
     */
    public function getFileLink($fieldData)
    {
        $url = Mage::getBaseUrl('media') . '/' .
            Juno_Customoptionfiles_Model_File::FOLDER_NAME . '/' .
            $fieldData->getFieldId() . '/' .
            $fieldData->getOptionValueId() . '/' .
            $fieldData->getFileName();

        return $url;
    }

    /**
     * Get file path
     *
     * @param $fieldData Juno_Customoptionfiles_Model_File
     * @return string
     */
    public function getFilePath($fieldData)
    {
        $path = Mage::getBaseDir('media') . DS .
            Juno_Customoptionfiles_Model_File::FOLDER_NAME . DS .
            $fieldData->getFieldId() . DS .
            $fieldData->getOptionValueId() . DS .
            $fieldData->getFileName();

        return $path;
    }

    /**
     * get model file by its property
     *
     * @param $fieldId
     * @param $optionValueId
     * @return null | Juno_Customoptionfiles_Model_File
     */
    public function getFileByFieldAndOptionValue($fieldId, $optionValueId)
    {
        $collection = Mage::getModel('customoptionfiles/file')->getCollection();
        $collection->addFieldToFilter('field_id', array('eq' => $fieldId));
        $collection->addFieldToFilter('option_value_id', array('eq' => $optionValueId));
        if ($collection->getFirstItem()->getId()) {
            return $collection->getFirstItem();
        }
        return null;
    }
}